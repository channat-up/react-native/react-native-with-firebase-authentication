/**
 * @format
 */

import {AppRegistry} from 'react-native';
import {name as appName} from './app.json';
import LoginSignUp from './src/screens/LoginSignUp';

AppRegistry.registerComponent(appName, () => LoginSignUp);
