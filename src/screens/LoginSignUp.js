import React, { Component } from "react";
import { Container, Header, Content, Form, Item, Input, Button, Text } from 'native-base';
// Firebase App (the core Firebase SDK) is always required and must be listed first
import * as firebase from "firebase/app";

// Add the Firebase products that you want to use
import "firebase/auth";
import "firebase/firestore";



export default class LoginSignUp extends Component {

    state = {
        email: '',
        password: ''
    }

    componentWillMount() {
        // TODO: Replace the following with your app's Firebase project configuration
        const firebaseConfig = {
            apiKey: "AIzaSyCa8R_xbIob_n3pGHrbEHuKJ0XUOgrQPr0",
            authDomain: "upauthfirebase-e7cfc.firebaseapp.com",
            databaseURL: "https://upauthfirebase-e7cfc.firebaseio.com",
            projectId: "upauthfirebase-e7cfc",
            storageBucket: "",
            messagingSenderId: "663420864916",
            appId: "1:663420864916:web:155f91661290c631"
        };
        
        // Initialize Firebase
        firebase.initializeApp(firebaseConfig);
    }
    


    signUp = () => {
        
        firebase.auth().createUserWithEmailAndPassword(this.state.email, this.state.password)
        .then(response => {
            // do something if it success 
            // e.g go to Login Screen, Home Screen
            console.log("res => :", response)
        })
        .catch(error => {
            console.log("erro => : ", error)
            alert(error.message)
        })
    }

    login = () => {
        firebase.auth().signInWithEmailAndPassword(this.state.email, this.state.password)
        .then(response => {
            // do something if it success 
            // e.g go to Login Screen, Home Screen
            console.log("res => :", response)
            alert("login success")
        })
        .catch(error => {
            console.log("erro => : ", error)
            alert(error.message)
        })
    }



  render() {
    return (
      <Container>
        <Header />
        <Content>
          <Form>
            <Item>
              <Input onChangeText={ (email) => this.state.email = email } 
              placeholder="Email" />
            </Item>
            <Item last>
              <Input onChangeText={ (password) => this.state.password = password } 
              secureTextEntry 
              placeholder="Password" />
            </Item>
          </Form>
          <Button onPress={this.signUp}>
              <Text>Signup</Text>
          </Button>
          <Button onPress={this.login}>
              <Text>Login</Text>
          </Button>
        </Content>
      </Container>
    );
  }
}
